﻿namespace StringSort.Model
{
    public record StringEntity(int Number, string Text)
    {
        public override string ToString()
        {
            return $"{Number}. {Text}";
        }
        public string Value => this.ToString();
    }
}
