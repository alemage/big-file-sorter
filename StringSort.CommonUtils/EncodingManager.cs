﻿using System;
using System.Text;

namespace StringSort.CommonUtils
{
    public class EncodingManager : IEncodingManager
    {
        public Encoding FileEncoding => Encoding.Unicode;
    }
}
