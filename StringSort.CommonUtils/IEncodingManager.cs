﻿using System.Text;

namespace StringSort.CommonUtils
{
    public interface IEncodingManager
    {
        Encoding FileEncoding { get; }
    }
}