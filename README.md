# big-file-sorter
Provides application to sort file of strings in the format "{number}. {text}"
Console application provides ability to sort files that not fit into memory

Currently supports only files in the Unicode. Can be changed manually in the EncodingManager

## FileSorter

### Settings
- [ ] InputFilePath - full pathname to file
- [ ] OutputFilePath - name of the sorted file
- [ ] MaxBytesToMemory - max bytes to use only in-memory sort
- [ ] MaxDegreeOfParallelismSort - maximum number of tasks to sort batches
- [ ] ChunkFileMaxSizeMb - size of the chunk in MB

- [ ] MergeChunksCount - count of chunks merged into one
- [ ] MaxDegreeOfParallelismMergeChunks - count of the tasks to merge chunks simultaniously 
- [ ] WriteBufferSize - buffer size to write chunks in the sorting part
- [ ] MergeBufferSize - buffer size to write chunks in the merging part
- [ ] KeepIntermediateFiles - flag to keep or delete files. Set as false to delete intermediate files

## FileCreatorTool 
Creates example of file with strings

### Input
One integer param - The length of created file in Mb

### Settings
- [ ] UniquenessPercent - approximate percent of duplicate strings
- [ ] CountOfRepeatedStrings - number of pre-created strings that used as duplicates
- [ ] StringLengthMin - min string length
- [ ] StringLengthMax - max string length
- [ ] WriteStreamBatchSizeInMb - batch to Stream Writer
- [ ] OutputFilePath - full output path to file (output.exe by default)

