﻿namespace StringSort.Sorter
{
    public record SorterSettings
    {
        public string InputFilePath { get; init; }
        public string OutputFilePath { get; init; }
        public int ChunkFileMaxSizeMb { get; init; }
        public int MaxBytesToMemory { get; init; }
        public int MaxDegreeOfParallelismSort { get; init; }

        public int WriteBufferSize { get; init; }
        public bool KeepIntermediateFiles { get; init; }


        public int MergeChunksCount { get; init; }
        public int MaxDegreeOfParallelismMergeChunks { get; init; }
        public int MergeBufferSize { get; init; }
    }
}