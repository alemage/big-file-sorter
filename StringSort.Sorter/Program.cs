﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace StringSort.Sorter
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello!");
            var settings = new SorterSettings
            {
                InputFilePath = "output_1Gb.txt",
                OutputFilePath = "sorted_strings_result.txt",
                MaxBytesToMemory = 100 * 1000 * 1000,
                MaxDegreeOfParallelismSort = 8,
                ChunkFileMaxSizeMb = 30,

                MergeChunksCount = 10,
                MaxDegreeOfParallelismMergeChunks = 8,
                WriteBufferSize = 8192,
                MergeBufferSize = 8192,
                KeepIntermediateFiles = false
            };

            var sorterFactory = new SorterFactory(settings);
            using (var sorter = sorterFactory.Create())
            {
                var cancellationToken = new CancellationToken();
                var now = DateTime.UtcNow;
                Console.WriteLine($"Start = {now}");
                Stopwatch sw = Stopwatch.StartNew();
                await sorter.SortStringsAsync(settings.InputFilePath, settings.OutputFilePath, cancellationToken);
                sw.Stop();
                var ms = sw.ElapsedMilliseconds;
                var ts = TimeSpan.FromMilliseconds(ms);
                now = DateTime.UtcNow;
                Console.WriteLine($"Start = {now}");
                Console.WriteLine($"Execution time = {ts}");
            }

            Console.WriteLine("Execution compeleted.");
            
        }
    }
}
