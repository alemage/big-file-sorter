﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using StringSort.CommonUtils;
using StringSort.Model;
using StringSort.Sorter.Extensions;

namespace StringSort.Sorter
{
    public class InMemorySorter : IStringSorter
    {        
        private readonly SorterSettings _settings;
        private readonly IComparer<StringEntity> _stringEntityComparer;
        private readonly IEncodingManager _encodingManager;

        public InMemorySorter(SorterSettings settings, IComparer<StringEntity> stringEntityComparer, IEncodingManager encodingManager)
        {
            _settings = settings;
            _stringEntityComparer = stringEntityComparer;
            _encodingManager = encodingManager;
        }

        public void Dispose() { }

        public async Task SortStringsAsync(string inFilePath, string outFilePath,  CancellationToken token)
        {
            var encoding = _encodingManager.FileEncoding;
            using FileStream fs = File.Open(inFilePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            using var sr = new StreamReader(fs, encoding);
            var lines = new List<StringEntity>();
            var rowNumber = 0;
            try
            {
                while (!sr.EndOfStream)
                {
                    rowNumber += 1;
                    var current = sr.ReadLine();              
                    lines.Add(current.ParseAsStringEntity());
                }

                lines.Sort(_stringEntityComparer);
                using FileStream writingStream = new FileStream(outFilePath, FileMode.CreateNew,
                 FileAccess.Write, FileShare.None, _settings.WriteBufferSize, FileOptions.None);
                using var sw = new StreamWriter(writingStream, encoding);
                foreach (var line in lines)
                {
                    await sw.WriteLineAsync(line.ToString().AsMemory(), token);
                }
                sw.Flush();
                sw.Close();
            }

            catch (Exception e)
            {
                Console.WriteLine($"{nameof(InMemorySorter.SortStringsAsync)}: Exception was thrown: {e.Message}");
                throw;
            }
        }
    }
}

                    