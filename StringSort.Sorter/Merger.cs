﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using StringSort.CommonUtils;
using StringSort.Model;
using StringSort.Sorter.Extensions;

namespace StringSort.Sorter
{
    public class Merger : IMerger
    {
        private readonly SorterSettings _settings;
        private readonly IComparer<StringEntity> _stringEntityComparer;
        private readonly IBatchNameProvider _batchNameProvidier;
        private readonly IEncodingManager _encodingManager;
        private readonly IFileCleaner _fileCleaner;
        private readonly object _sync = new object();

        public Merger(
            SorterSettings settings,
            IComparer<StringEntity> stringEntityComparer,
            IBatchNameProvider batchNameProvidier,
            IEncodingManager encodingManager,
            IFileCleaner fileCleaner)
        {
            _settings = settings;
            _stringEntityComparer = stringEntityComparer;
            _batchNameProvidier = batchNameProvidier;
            _encodingManager = encodingManager;
            _fileCleaner = fileCleaner;
        }

        public async Task<string[]> MergeFiles(string[] filesToMerge, int iteration, string batchName, CancellationToken token)
        {
            await Task.CompletedTask;
            var page = 0;            
            var resultFiles = new ConcurrentBag<string>();
            if (filesToMerge.Length == 1)
            {
                RenameFile(filesToMerge[0], batchName);
                return new string[] { batchName };
            }

            var batch = _settings.MergeChunksCount;
            var fileBatches = new List<(List<string>, int)>();
            var currentBatch = filesToMerge.Skip(page * batch).Take(batch).ToList();
            while (currentBatch.Any())
            {
                fileBatches.Add((currentBatch, page));
                page++;
                currentBatch = filesToMerge.Skip(page * batch).Take(batch).ToList();
            }

            var allTasks = new ConcurrentBag<Task>();
            using var semaphore = new SemaphoreSlim(_settings.MaxDegreeOfParallelismMergeChunks, _settings.MaxDegreeOfParallelismMergeChunks);
            foreach(var (filesToProcess, filePage) in fileBatches)
            {
                await semaphore.WaitAsync();
                Task currentTask;
                lock(_sync)
                {
                    var outputFileName = _batchNameProvidier.GetBatchName(batchName, iteration, filePage);
                    if (filesToProcess.Count == 1)
                    {
                        RenameFile(filesToProcess[0], outputFileName);
                        filesToProcess.Clear();
                        currentTask = Task.CompletedTask;
                    }
                    else
                    {
                        currentTask = MergeStreams(filesToProcess, outputFileName, token);
                        resultFiles.Add(outputFileName);
                    }
                }

                allTasks.Add(currentTask);
                var _ = currentTask.ContinueWith((t)=>
                {
                    semaphore.Release();
                });                
            }

            await Task.WhenAll(allTasks);
            _fileCleaner.AddFilesToRemove(filesToMerge);
            var recursiveResult = await MergeFiles(resultFiles.ToArray(), iteration += 1, batchName, token);
            return recursiveResult;
        }

        private void RenameFile(string currentName, string newName)
        {
            File.Move(currentName, newName);
        }

        private async Task MergeStreams(List<string> filesToProcess, string outputFileName, CancellationToken token)
        {
            var encoding = _encodingManager.FileEncoding;
            var (streamReaders, data) = await GetStreamReaders(filesToProcess);

            var finished = new List<int>(streamReaders.Length);
            var done = false;

            using FileStream writingStream = new FileStream(outputFileName, FileMode.CreateNew,
                FileAccess.Write, FileShare.None, _settings.WriteBufferSize, FileOptions.None);
            using var sw = new StreamWriter(writingStream, encoding);

            while (!done)
            {
                data.Sort((v1,v2) => _stringEntityComparer.Compare(v1.Item1,v2.Item1));
                var (current, streamReaderIndex) = data[0];
                var valueToWrite = current.ToString();

                await sw.WriteLineAsync(valueToWrite.AsMemory(), token);

                if (streamReaders[streamReaderIndex].EndOfStream)
                {
                    var indexToRemove = data.FindIndex( streamData => streamData.Item2 == streamReaderIndex);
                    data.RemoveAt(indexToRemove);
                    finished.Add(streamReaderIndex);
                    done = finished.Count == streamReaders.Length;
                }
                else
                {
                    var rawValue = await streamReaders[streamReaderIndex].ReadLineAsync();
                    var parsedValue = rawValue.ParseAsStringEntity();
                    data[0] = (parsedValue, streamReaderIndex);
                }
            }
        }

        private async Task<(StreamReader[] StreamReaders, List<(StringEntity,int)> rows)> GetStreamReaders(List<string> sortedFiles)
        {
           var encoding = _encodingManager.FileEncoding;
           var streamReaders = new StreamReader[sortedFiles.Count];
           var firstRows = new List<(StringEntity, int)>(sortedFiles.Count);
           for (var i = 0; i < sortedFiles.Count; i++)
           {
               var sortedFilePath = sortedFiles[i];
               var sortedFileStream = File.OpenRead(sortedFilePath);
               streamReaders[i] = new StreamReader(sortedFileStream, encoding, bufferSize: _settings.MergeBufferSize);
               var value = await streamReaders[i].ReadLineAsync();
               var stringSort = value.ParseAsStringEntity();
               var row = (stringSort, i);

               firstRows.Add(row);
           }

           return (streamReaders, firstRows);
        }
    }
}
