﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace StringSort.Sorter
{
    public interface IFileCleaner
    {
        void AddFilesToRemove(string[] filesToRemove);
        void RunCleanup();
    }
}