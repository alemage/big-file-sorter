﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace StringSort.Sorter
{
    public class ExternalSorter : IStringSorter
    {
        private readonly SorterSettings _settings;
        private readonly IDataSplitter _dataSplitter;
        private readonly IStringSorter _inMemorySorter;
        private readonly IMerger _merger;
        private readonly IBatchNameProvider _batchNameProvider;
        private readonly IFileCleaner _fileCleaner;
        private readonly object _sync = new object();

        public ExternalSorter(
            SorterSettings settings,
            IDataSplitter dataSplitter,
            IStringSorter inMemorySorter,
            IMerger merger,
            IBatchNameProvider batchNameProvider,
            IFileCleaner fileCleaner
            )
        {
            _settings = settings;
            _dataSplitter = dataSplitter;
            _inMemorySorter = inMemorySorter;
            _merger = merger;
            _batchNameProvider = batchNameProvider;
            _fileCleaner = fileCleaner;
        }

        

        public async Task SortStringsAsync(string inFilePath, string outFilePath, CancellationToken token)
        {
            var batchName = outFilePath;
            var iteration = 0;
            IAsyncEnumerable<string> batchesToSort =  _dataSplitter.SplitIntoBatchFilesAsync(inFilePath, outFilePath, token);

            iteration += 1;
            var filesToMerge = await SortInitialChunks(batchesToSort, iteration, batchName, token);

            iteration += 1;
            await _merger.MergeFiles(filesToMerge, iteration, batchName, token);
            
        }

        public void Dispose()
        {
            if (!_settings.KeepIntermediateFiles)
            {
                _fileCleaner.RunCleanup();
            };
        }


        private async Task<string[]> SortInitialChunks(
            IAsyncEnumerable<string> chunks,
            int iteration,
            string outputFilePath,
            CancellationToken token)
        {
            var outputFiles = new ConcurrentBag<string>();  
            var allFileNames = new ConcurrentBag<string>();            
            var allTasks = new ConcurrentBag<Task>();


            using var semaphore = new SemaphoreSlim(_settings.MaxDegreeOfParallelismSort, _settings.MaxDegreeOfParallelismSort);
            var index = -1;
            await foreach (var fileName in chunks.WithCancellation(token).ConfigureAwait(false))
            {
                allFileNames.Add(fileName);
                await semaphore.WaitAsync();
                lock (_sync)
                {
                    index++;
                    string outputFileName = _batchNameProvider.GetBatchName(outputFilePath, iteration, index);
                    outputFiles.Add(outputFileName);
                    var task = _inMemorySorter.SortStringsAsync(fileName, outputFileName, token);
                    task.ContinueWith((t) =>
                    {
                        semaphore.Release();
                    }, token);
                    allTasks.Add(task);
                }                               
            }

            await Task.WhenAll(allTasks);
            _fileCleaner.AddFilesToRemove(allFileNames.ToArray());
            return outputFiles.ToArray();
        }      
    }
}
