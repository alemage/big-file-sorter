﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace StringSort.Sorter
{
    public interface IStringSorter : IDisposable
    {
        Task SortStringsAsync(string inFilePath, string outFilePath, CancellationToken token);
    }
}