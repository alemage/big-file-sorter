﻿using System.Threading;
using System.Threading.Tasks;

namespace StringSort.Sorter
{
    public interface IMerger
    {
        Task<string[]> MergeFiles(string[] filesToMerge, int iteration, string batchName, CancellationToken token);
    }
}