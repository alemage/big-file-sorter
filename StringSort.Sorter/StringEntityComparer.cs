﻿using System;
using System.Collections.Generic;
using StringSort.Model;

namespace StringSort.Sorter
{
    public class StringEntityComparer : IComparer<StringEntity>
    {
        public int Compare(StringEntity left, StringEntity right)
        {
            if (left.Text.Equals(right.Text))
            {
                return left.Number.CompareTo(right.Number);
            }
            else
            {
                return left.Text.CompareTo(right.Text);
            }
        }
    }
}
