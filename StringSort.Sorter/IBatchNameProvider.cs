﻿namespace StringSort.Sorter
{
    public interface IBatchNameProvider
    {
        string GetBatchName(string basePath, int iteration, int page);
    }
}