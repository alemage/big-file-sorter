﻿using System;
namespace StringSort.Sorter
{
    public class BatchNameProvider : IBatchNameProvider
    {
        public string GetBatchName(string basePath, int iteration, int page)
        {
            return $"{basePath}-{iteration}-{page}.batch";
        }

    }
}
