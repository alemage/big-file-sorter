﻿using System.Collections.Generic;
using System.Threading;

namespace StringSort.Sorter
{
    public interface IDataSplitter
    {
        IAsyncEnumerable<string> SplitIntoBatchFilesAsync(string inFilePath, string outFilePath, CancellationToken token);
    }
}