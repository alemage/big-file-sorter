﻿using System;
using StringSort.Model;

namespace StringSort.Sorter.Extensions
{
    public static class StringExtensions
    {
        private const char BomChar = (char)65279;
        public static StringEntity ParseAsStringEntity(this string val)
        {
            var parts = val.Trim(BomChar).Split(". ", 2);
            if (parts.Length != 2)
            {
                throw new Exception($"{nameof(InMemorySorter.SortStringsAsync)}: Incorrect input data format in row '{val}'");
            }

            if (!int.TryParse(parts[0], out int priorityNum))
            {
                throw new Exception($"{nameof(InMemorySorter.SortStringsAsync)}: Incorrect input data format in row '{val}'");
            }

            return new StringEntity(priorityNum, parts[1]);
        }
    }
}
