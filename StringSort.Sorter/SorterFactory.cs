﻿using StringSort.CommonUtils;

namespace StringSort.Sorter
{
    public class SorterFactory
    {
        private readonly SorterSettings _settings;

        public SorterFactory(SorterSettings settings)
        {
            _settings = settings;
        }

        public IStringSorter Create()
        {
            var path = _settings.InputFilePath;
            long lengthBytes = new System.IO.FileInfo(path).Length;
            var stringEntityComparer = new StringEntityComparer();
            var encodingManager = new EncodingManager();
            var InMemorySorter = new InMemorySorter(_settings, stringEntityComparer, encodingManager);
            if (FitIntoMemory(lengthBytes))
            {
                return InMemorySorter;
            }

            var batchNameProvider = new BatchNameProvider();
            var dataSplitter = new DataSplitter(_settings, batchNameProvider, encodingManager);
            var fileCleaner = new FileCleaner();
            var merger = new Merger(_settings, stringEntityComparer, batchNameProvider, encodingManager, fileCleaner);
            return new ExternalSorter(_settings, dataSplitter, InMemorySorter, merger, batchNameProvider, fileCleaner);
        }

        private bool FitIntoMemory(long lengthBytes)
        {
            return (lengthBytes < _settings.MaxBytesToMemory);
        }
    }
}
