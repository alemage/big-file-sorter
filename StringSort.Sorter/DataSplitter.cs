﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using StringSort.CommonUtils;

namespace StringSort.Sorter
{
    public class DataSplitter : IDataSplitter
    {
        private readonly SorterSettings _settings;
        private readonly IBatchNameProvider _batchNameProvider;
        private readonly IEncodingManager _encodingManager;

        public DataSplitter(SorterSettings settings, IBatchNameProvider batchNameProvider, IEncodingManager encodingManager)
        {
            _settings = settings;
            _batchNameProvider = batchNameProvider;
            _encodingManager = encodingManager;
        }
        
        public async IAsyncEnumerable<string> SplitIntoBatchFilesAsync(string inFilePath, string outFilePath, [EnumeratorCancellation] CancellationToken token)
        {
            var bufferSize = _settings.ChunkFileMaxSizeMb * 1000 * 1000;
            var encoding = _encodingManager.FileEncoding;
            var batchNames = new List<string>();
            using FileStream fs = File.Open(inFilePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            using var sr = new StreamReader(fs, encoding);
            var page = 0;
            var iteration = 0;
            while (!sr.EndOfStream)
            {
                var batchFileName = _batchNameProvider.GetBatchName(outFilePath, iteration, page);
                batchNames.Add(batchFileName);
                using var sw = new StreamWriter(batchFileName, false, encoding);
                var handledBytes = 0;

                while (!sr.EndOfStream && handledBytes <= bufferSize)
                {
                    string current = sr.ReadLine();
                    var byteSize = encoding.GetBytes(current);
                    handledBytes += byteSize.Length;
                    await sw.WriteLineAsync(current);
                }

                sw.Flush();
                sw.Close();
                page += 1;
                yield return batchFileName;
            }
            sr.Close();
        }
    }
}
