﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace StringSort.Sorter
{
    public class FileCleaner : IFileCleaner
    {
        private readonly ConcurrentBag<string[]> _batchToRemove = new ConcurrentBag<string[]>();

        public void AddFilesToRemove(string[] filesToRemove)
        {
            _batchToRemove.Add(filesToRemove);
        }

        public void RunCleanup()
        {
            RunCleanupAsync().ConfigureAwait(false).GetAwaiter().GetResult();
        }

        private async Task RunCleanupAsync()
        {
            var allTasks = new ConcurrentBag<Task>();
            foreach (var batch in _batchToRemove)
            {
                var task = Task.Run(() =>
                {
                    foreach (var fileName in batch)
                    {
                        RemoveFile(fileName);
                    }
                });
                allTasks.Add(task);
            }
            await Task.WhenAll(allTasks);
        }

        private void RemoveFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
        }
    }
}
