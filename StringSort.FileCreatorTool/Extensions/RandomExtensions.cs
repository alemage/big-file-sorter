﻿using System;
using System.Collections.Generic;

namespace StringSort.FileCreatorTool.Extensions
{
    public static class RandomExtensions
    {
        public static IEnumerable<string> GetRandomStrings(this Random rand, string allowedChars, (int min, int max) length, int count, string restrictedFirstChars = "")
        {
            //TODO: add inputValidation
            var usedRandomStrings = new HashSet<string>();
            (int min, int max) = length;
            char[] chars = new char[max];
            int setLength = allowedChars.Length;
            var allowedFirstCharArray = new List<char>();
            foreach (char ch in allowedChars)
            {
                if (!restrictedFirstChars.Contains(ch))
                {
                    allowedFirstCharArray.Add(ch);
                }
            }
            int firstCharSetLength = allowedFirstCharArray.Count;

            while (count > 0)
            {
                int stringLength = rand.Next(min, max + 1);

                chars[0] = allowedFirstCharArray[rand.Next(firstCharSetLength)];

                for (int i = 1; i < stringLength; ++i)
                {
                    chars[i] = allowedChars[rand.Next(setLength)];
                }

                string randomString = new string(chars, 0, stringLength);

                if (usedRandomStrings.Add(randomString))
                {
                    yield return randomString;
                }
                else
                {
                    count++;
                }

                count--;
            }
        }
    }
}
