﻿namespace StringSort.FileCreatorTool
{
    public interface IStringEntityGenerator
    {
        void GenerateEntities(int size);
    }
}
