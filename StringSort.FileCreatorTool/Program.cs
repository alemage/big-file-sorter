﻿using System;
using StringSort.CommonUtils;

namespace StringSort.FileCreatorTool
{
    class Program
    {
        private const int MaxFileSize = 100000;
        static void Main(string[] args)
        {
            //TODO: Extract validation, use named parameters
            if (args.Length !=1 )
            {
                Console.WriteLine($"Wrong number of params. Shold be 1 - the size of the file in MB as number from 1 to {MaxFileSize}");
                return;
            }

            var sizeStr = args[0];
            if (!int.TryParse(sizeStr, out var size)){
                Console.WriteLine("Wrong number of params. Should be: first parameter - the size of the file in MB");
                return;
            }
            var (isValid, errorMessage) = ValidateSizeRange(size);
            if (!isValid)
            {
                Console.WriteLine(errorMessage);
            }

            //TODO: Use console params or config file
            var settings = new EntityLanguageSettings {
                UniquenessPercent = 30,
                CountOfRepeatedStrings = 100,
                StringLengthMin = 16,
                StringLengthMax = 128,
                WriteStreamBatchSizeInMb = 100,
                OutputFilePath = "output.txt"
            };

            var encodingManager = new EncodingManager();
            var stringEntityGenerator = new StringEntityWriter(settings, encodingManager);
            stringEntityGenerator.GenerateEntities(size);
        }

        private static (bool isValid, string errorMessage) ValidateSizeRange(int size)
        {
            if (size < 1 || size > MaxFileSize){
                return (false, $"{nameof(Program.ValidateSizeRange)}: The size '{size}' not in range from '1' to '{MaxFileSize}'");
            }
            
            return (true, null);
        }
    }
}
