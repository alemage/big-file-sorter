﻿namespace StringSort.FileCreatorTool
{
    public record EntityLanguageSettings
    {
        public int UniquenessPercent { get; init; }
        public int CountOfRepeatedStrings { get; init; }
        public int StringLengthMin { get; init; }
        public int StringLengthMax { get; init; }
        public int WriteStreamBatchSizeInMb { get; init; }
        public string OutputFilePath { get; init; }
    }
}
