﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using StringSort.CommonUtils;
using StringSort.Model;

namespace StringSort.FileCreatorTool
{
    public class StringEntityWriter : IStringEntityGenerator
    {
        private readonly EntityLanguageSettings _settings;
        private readonly IEncodingManager _encodingManager;
        private readonly EntityCreator _entityCreator;

        public StringEntityWriter(EntityLanguageSettings settings, IEncodingManager encodingManager)
        {
            _settings = settings;
            _encodingManager = encodingManager;
            _entityCreator = new EntityCreator(settings, encodingManager);
        }

        //TODO: could use Record for Size like - Size(10, MeasureUnit.Megabytes) 
        public void GenerateEntities(int sizeMb)
        {
            var uniqueFactor = Math.Max(1, Math.Min(100, _settings.UniquenessPercent));
            var generatedCount = _entityCreator.GeneratedCount;
            var batchSizeBytes = _entityCreator.GeneratedSize;
            var approximateMegabytes = batchSizeBytes / Math.Pow(10, 6);
            if (sizeMb < approximateMegabytes)
            {
                throw new ArgumentException($"{nameof(StringEntityWriter)}: the size of the file is too small");
            }

            var approximateBatchCount = generatedCount / uniqueFactor * 100;

            Func<int?, IList<StringEntity>> entityFactory = _entityCreator.GenerateEntities;

            DeleteOutputIfExists();

            long fileSizeInBytes = (long) sizeMb * 1000 * 1000;
            while (fileSizeInBytes > 0)
            {
                fileSizeInBytes -= WriteEntitiesToFile(entityFactory, approximateBatchCount, fileSizeInBytes);
            }
        }

        private long WriteEntitiesToFile(Func<int?, IList<StringEntity>> entityFactory, int batchSize, long expectedSizeBytes)
        {
            var encoding = _encodingManager.FileEncoding;
            using StreamWriter sw = new StreamWriter(_settings.OutputFilePath, true, encoding);
            long writedBytes = 0;
            long bytesToWrite = Math.Min(_settings.WriteStreamBatchSizeInMb * 1000 * 1000, expectedSizeBytes);
            while (writedBytes < bytesToWrite)
            {
                var currentBatch = entityFactory(batchSize);
                foreach (var entity in currentBatch)
                {
                    var strRes = $"{entity.ToString()}{Environment.NewLine}";
                    byte[] bytes = encoding.GetBytes(strRes);
                    writedBytes += bytes.Length;
                    sw.BaseStream.Write(bytes);
                }
                
            }

            sw.Flush();
            sw.Close();
            return writedBytes;
        }

        private void DeleteOutputIfExists()
        {
            var path = _settings.OutputFilePath;
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }
    }
}
