﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StringSort.CommonUtils;
using StringSort.FileCreatorTool.Extensions;
using StringSort.Model;

namespace StringSort.FileCreatorTool
{
    public class EntityCreator
    {
        private const string AlowedChars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz#@$^*() ";
        private const string RestrictedFirstChars = "0123456789#@$^*() ";
        private readonly EntityLanguageSettings _settings;
        private readonly IEncodingManager _encodingManager;
        private List<string> _repeatedStrings = new List<string>();
        public EntityCreator(EntityLanguageSettings settings, IEncodingManager encodingManager)
        {
            _settings = settings;
            _encodingManager = encodingManager;
            GenerateLanguage();
        }

        public IList<StringEntity> GenerateEntities(int? count)
        {
            var rand = new Random();

            if (count == null)
            {
                count = _repeatedStrings.Count;
            }

            var result = new List<StringEntity>(count.Value);
            for (var i = 0; i < _repeatedStrings.Count && i < count; i++)
            {                 
                var entity = GetNextStringEntity(rand, _repeatedStrings[i]);
                result.Add(entity);
            }

            count -= result.Count;
            if (count > 0)
            {
                var randomStrings = GetRandomStrings(count.Value).ToList();
                foreach(var randomString in randomStrings)
                {
                    StringEntity entity = GetNextStringEntity(rand, randomString);
                    result.Add(entity);
                }
            }

            return result;

            StringEntity GetNextStringEntity(Random rand, string randomString)
            {
                var num = rand.Next(_settings.StringLengthMin, _settings.StringLengthMax);
                var entity = new StringEntity(num, randomString);
                return entity;
            }
        }

        public int GeneratedCount => _repeatedStrings.Count;

        public int GeneratedSize { get; private set; }


        private IEnumerable<string> GetRandomStrings(int count)
        {
            var rand = new Random();
            return rand.GetRandomStrings(AlowedChars, (_settings.StringLengthMin, _settings.StringLengthMax), count, RestrictedFirstChars);
        }

        private void GenerateLanguage()
        {
            var encoding = _encodingManager.FileEncoding;
            var repeatedStrings = GetRandomStrings(_settings.CountOfRepeatedStrings);
            _repeatedStrings = repeatedStrings.ToList();
            int byteSize = 0;
            foreach (var s in _repeatedStrings)
            {
                byte[] stringBytes = encoding.GetBytes(s);
                byteSize += stringBytes.Length;
            }
            GeneratedSize = byteSize;
        }
    }
}
